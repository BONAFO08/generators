<?php


// function mysql_connect($DBNAME, Int $PORT = 3306, $HOST = "localhost", $USER = "root", $PASSWORD = "")
// {
//     $dbname = $DBNAME;
//     $host = $HOST;
//     $user = $USER;
//     $password = $PASSWORD;
//     $port = $PORT;

//     try {
//         $conexion = new PDO(
//             "mysql:
//         host=$host;
//         port=$port;
//         dbname=$dbname; 
//     ",
//             $user,
//             $password
//         );

//         $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//         return $conexion;
//     } catch (\Throwable $error) {
//         error_log($error);
//         print_r($error);
//     }
// }


//EXAMPLE OF USE =>   [ postgres_connect ] <= 
// postgres_connect('Users',5432,'localhost','postgres','myFavoritePassword');

function postgres_connect($DBNAME, Int $PORT = 5432, $HOST = "localhost", $USER = "postgres", $PASSWORD = "123")
{
    $dbname = $DBNAME;
    $host = $HOST;
    $user = $USER;
    $password = $PASSWORD;
    $port = $PORT;

    try {
        $conexion = new PDO(
            "pgsql:
        host=$host;
        port=$port;
        dbname=$dbname; 
    ",
            $user,
            $password
        );

        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conexion;
    } catch (\Throwable $error) {
        error_log($error);
        print_r($error);
    }
}





//EXAMPLE OF USE =>   [ postgres_colum_generator ] <= 
// $data = (object) [
//     'id' => postgres_colum_generator('serial','PRIMARY KEY'),
//     'name' => postgres_colum_generator('character varying(50)','NOT NULL UNIQUE'),
//     'age' => postgres_colum_generator('integer','NOT NULL'),
// ];

function postgres_colum_generator($type, $extra_prop)
{
    return (object) ['type' =>  $type, 'extra_prop' => $extra_prop];
}




function postgres_generator_create_table($table_name, $data_arr)
{
    $part1 = "CREATE TABLE IF NOT EXISTS $table_name(";
    $part2 = "";
    $part3 = ")";

    for ($i = 0; $i < sizeof($data_arr); $i++) {
        if ($i == (sizeof($data_arr) - 1)) {
            $part2 = $part2 . $data_arr[$i]->key . ' ' . $data_arr[$i]->value->type . ' ' . $data_arr[$i]->value->extra_prop;
        } else {
            $part2 = $part2 . $data_arr[$i]->key . ' ' . $data_arr[$i]->value->type . ' ' . $data_arr[$i]->value->extra_prop . ',';
        }
    }

    return $part1 . $part2 . $part3;
}





//EXAMPLE OF USE =>   [ postgres_generator_insert_colum ] <= 
// $sql_instruction_insert_colum = postgres_generator_insert_colum('Users','married','bool NOT NULL UNIQUE');

function postgres_generator_insert_colum($table_name, $column_name, $type)
{
    return "ALTER TABLE $table_name ADD COLUMN IF NOT EXISTS $column_name $type;";
}



//EXAMPLE OF USE =>   [ postgres_generator_delete_colum ] <= 
// $sql_instruction_delete_colum = postgres_generator_delete_colum('User','married');

function postgres_generator_delete_colum($table_name, $column_name)
{
    return "ALTER TABLE $table_name DROP COLUMN IF EXISTS $column_name;";
}


//EXAMPLE OF USE =>   [ postgres_generator_rename_colum ] <= 
// $sql_instruction_delete_colum = postgres_generator_rename_colum('User','marryed','married');

function postgres_generator_rename_colum($table_name, $column_name, $new_name)
{
    return "ALTER TABLE $table_name RENAME COLUMN $column_name TO $new_name;";
}





//EXAMPLE OF USE =>   [ postgres_generator_modify_colum ] <= 
// $sql_instruction_modify_colum = postgres_generator_modify_colum('User','name','TYPE varchar(30) SET NOT NULL');

function postgres_generator_modify_colum($table_name, $column_name, $modification)
{
    return "ALTER TABLE $table_name ALTER COLUMN $column_name $modification;";
}





function postgres_generator_insert($table_name, $data_arr)
{
    $part1 = "INSERT INTO $table_name (";
    $part2 = "";
    $part3 = ") VALUES (";
    $part4 = "";
    $part5 = ");";

    for ($i = 0; $i < sizeof($data_arr); $i++) {
        if ($i == (sizeof($data_arr) - 1)) {
            $part2 = $part2 . $data_arr[$i]->key;
            $part4 = $part4 . postgres_code_generator($data_arr[$i]->value);
        } else {
            $part2 = $part2 . $data_arr[$i]->key . ',';
            $part4 = $part4 . postgres_code_generator($data_arr[$i]->value) . ',';
        }
    }

    return $part1 . $part2 . $part3 . $part4 . $part5;
}




//EXAMPLE OF USE =>   [ postgres_generator_delete ] <= 
// $sql_instruction_delete = postgres_generator_delete('User', "name = 'Naomi'");
function postgres_generator_delete($table_name, $condition)
{
    return "DELETE FROM $table_name WHERE $condition ;";
}




//EXAMPLE OF USE =>   [ postgres_generator_delete ] <= 
// $sql_instruction_search_all = postgres_generator_search_all('Users');
function postgres_generator_search_all($table_name)
{
    return "SELECT * FROM $table_name;";
}

//EXAMPLE OF USE =>   [ postgres_generator_delete ] <= 
// $sql_instruction_delete = postgres_generator_delete('User', "name = 'Naomi'");
function postgres_generator_search($table_name, $condition)
{
    return "SELECT * FROM $table_name WHERE $condition;";
}




function postgres_code_generator($data)
{
    if (!is_object($data)) {
        $data = (object) [
            'data' => $data, 'type' => ''
        ];
    }
    if (!property_exists($data, 'type')) {
        $data->type = '';
    }


    switch ($data->type) {
        case 'varchar':
        case 'character varying':
            return  "'" . $data->data . "'";
            break;

        default:
            return $data->data;
            break;
    }
}





function postgres_generator_update($table_name, $data_arr, $condition)
{
    $part1 = "UPDATE $table_name SET ";
    $part2 = "";
    $part3 = " WHERE";
    $part4 = " $condition;";

    for ($i = 0; $i < sizeof($data_arr); $i++) {
        if ($i == (sizeof($data_arr) - 1)) {
            $part2 = $part2 . $data_arr[$i]->key . "=" . postgres_code_generator($data_arr[$i]->value);
        } else {
            $part2 = $part2 . $data_arr[$i]->key . "=" . postgres_code_generator($data_arr[$i]->value) . ',';
        }
    }

    return $part1 . $part2 . $part3 . $part4;
}




function postgres_send($postgres_connection, $postgres_instruction)
{
    try {
        $preparation_instruction = $postgres_connection->prepare($postgres_instruction);
        $response = $preparation_instruction->execute();
        $response =  $preparation_instruction->fetchAll();
        return (object) [
            'msj' => $response,
            'boolean' => true
        ];
    } catch (\Throwable $error) {
        return (object) [
            'msj' => $error,
            'boolean' => false
        ];
    }
}


